package sbp.branching;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import sbp.common.Utils;

import static org.mockito.ArgumentMatchers.anyString;

public class MyBranchingTest {

    /**
     * На этот метод необходимо написать unit-тест
     * Тест должен содержать mock на объект {@link Utils} (реализация неизвестна)
     * Необходимо проверить результат выполнения метода в зависимости от возможных реализаций {@link Utils}
     *
     * @param i1 - первое значение
     * @param i2 - второе значение
     * @return - большее из значений или 0, если Utils#utilFunc2 вернёт true;
     */
    @Test
    public void maxIntTestFunc2True() {
        final int i1 = 3;
        final int i2 = 4;
        Utils utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(true);
        MyBranching myBranching = new MyBranching(utilsMock);
        int result = myBranching.maxInt(i1, i2);
        Assertions.assertEquals(0,result);
    }
    @Test
    public void maxIntTestFunc2False() {
        final int i1 = 3;
        final int i2 = 4;
        Utils utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(false);
        MyBranching myBranching = new MyBranching(utilsMock);
        int result = myBranching.maxInt(i1, i2);
        Assertions.assertEquals(Math.max(i1,i2),result);
    }

    /**
     * На этот метод необходимо написать unit-тест
     * Тест должен содержать mock на объект {@link Utils} (реализация неизвестна)
     * Необходимо проверить результат выполнения метода в зависимости от возможных реализаций {@link Utils}
     * @return - true, если Utils#utilFunc2() возвращает true
     */
    @Test
    public void ifElseExample_Successful() {
        Utils utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(true);
        MyBranching myBranching = new MyBranching(utilsMock);
        boolean result = myBranching.ifElseExample();
        Assertions.assertTrue(result);
    }
    @Test
    public void ifElseExample_Fail() {
        Utils utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(false);
        MyBranching myBranching = new MyBranching(utilsMock);
        boolean result = myBranching.ifElseExample();
        Assertions.assertFalse(result);
    }
    /**
     * На этот метод необходимо написать unit-тест
     * Тест должен содержать mock на объект {@link Utils} (реализация неизвестна)
     * Необходимо проверить выполнение Utils#utilFunc1 и Utils#utilFunc2 при входящем i = 0
     * Необходимо проверить выполнение Utils#utilFunc1 и Utils#utilFunc2 при входящем i = 1
     * Необходимо проверить выполнение Utils#utilFunc1 и Utils#utilFunc2 при входящем i = 2
     * Можно сделать все проверки одним тестом, но лучше под каждую проверку завести отдельно тест
     * @param i - входящее значение (может быть любым)
     */

    /**
     * Тесты при входящем значении i=0
     */
    @Test
    public void switchExample_i0_Func2True(){
        final int i = 0;
        Utils utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(true);
        MyBranching myBranching = new MyBranching(utilsMock);
        myBranching.switchExample(i);
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc1(anyString());
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();
    }
    @Test
    public void switchExample_i0_Func2False(){
        final int i = 0;
        Utils utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(false);
        MyBranching myBranching = new MyBranching(utilsMock);
        myBranching.switchExample(i);
        Mockito.verify(utilsMock, Mockito.never()).utilFunc1(anyString());
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();
    }
    /**
     * Тесты при входящем значении i=1
     */
    @Test
    public void switchExample_i1(){
        final int i = 1;
        Utils utilsMock = Mockito.mock(Utils.class);
        MyBranching myBranching = new MyBranching(utilsMock);
        myBranching.switchExample(i);
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc1(anyString());
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();
    }

    /**
     * Тесты при входящем значении i=2
     */
    @Test
    public void switchExample_i2(){
        final int i = 2;
        Utils utilsMock = Mockito.mock(Utils.class);
        MyBranching myBranching = new MyBranching(utilsMock);
        myBranching.switchExample(i);
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();
        Mockito.verify(utilsMock, Mockito.never()).utilFunc1(anyString());
    }
}
